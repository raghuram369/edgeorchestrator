package orchestrator

import (
	"net/http"

	"github.com/raghu/EdgeOrchestrator/logger"

	"github.com/gorilla/mux"
)

var controller = &Controller{Repository: Repository{}}

//Route definition
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

//Routes defines the list of routes of our API
type Routes []Route

var routes = Routes{
	Route{
		"Index",
		"POST",
		"/",
		controller.Index,
	},
	Route{
		"DeploySites",
		"POST",
		"/addapp",
		controller.AddApp,
	},
	Route{
		"ActivateHelm",
		"POST",
		"/activate",
		controller.ActivateHelm,
	},
	Route{
		"DeactivateHelm",
		"POST",
		"/deactivate",
		controller.DeactivateHelm,
	},
	Route{
		"UpdateHelm",
		"PUT",
		"/update",
		controller.UpdateHelm,
	},
	Route{
		"RollbackHelm",
		"PUT",
		"/rollback",
		controller.RollbackHelm,
	},
	Route{
		"PurgeHelm",
		"DELETE",
		"/delete",
		controller.PurgeHelm,
	},
}

//NewRouter configures a new router to the API
func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		var handler http.Handler
		handler = route.HandlerFunc
		handler = logger.Logger(handler, route.Name)

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)

	}
	return router
}

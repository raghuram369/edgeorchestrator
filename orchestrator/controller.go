package orchestrator

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	pb "github.com/raghu/EdgeOrchestrator/manager"
)

//Controller ...
type Controller struct {
	Repository Repository
}

const (
	address = "localhost:50051"
)

// Index GET /
func (c *Controller) Index(w http.ResponseWriter, r *http.Request) {
	apps := c.Repository.GetApps() // list of all apps
	log.Println(apps)
	data, _ := json.Marshal(apps)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	w.Write(data)
	return
}

// AddApp POST /
func (c *Controller) AddApp(w http.ResponseWriter, r *http.Request) {

	var app App
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576)) // read the body of the request
	if err != nil {
		log.Fatalln("Error AddApp", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if err := r.Body.Close(); err != nil {
		log.Fatalln("Error AddApp", err)
	}
	if err := json.Unmarshal(body, &app); err != nil { // unmarshall body contents as a type Candidate
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Fatalln("Error AddApp unmarshalling data", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
	success := c.Repository.AddApp(app) // adds the app to the DB
	log.Println(success)
	if !success {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	return
}

// UpdateApp PUT /
func (c *Controller) UpdateApp(w http.ResponseWriter, r *http.Request) {
	var app App
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576)) // read the body of the request
	if err != nil {
		log.Fatalln("Error UpdateApp", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if err := r.Body.Close(); err != nil {
		log.Fatalln("Error AddaUpdateApp", err)
	}
	if err := json.Unmarshal(body, &app); err != nil { // unmarshall body contents as a type Candidate
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Fatalln("Error UpdateApp unmarshalling data", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	success := c.Repository.UpdateApp(app) // updates the app in the DB
	if !success {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	return
}

// DeleteApp DELETE /
func (c *Controller) DeleteApp(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"] // param id

	if err := c.Repository.DeleteApp(id); err != "" { // delete a app by id
		if strings.Contains(err, "404") {
			w.WriteHeader(http.StatusNotFound)
		} else if strings.Contains(err, "500") {
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	//w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)

	return
}

//ActivateHelm chart
func (c *Controller) ActivateHelm(w http.ResponseWriter, r *http.Request) {

	var activateHelmRequest *pb.HelmActivateRequest
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576)) // read the body of the request
	if err != nil {
		log.Fatalln("Error Activating helm", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if err := r.Body.Close(); err != nil {
		log.Fatalln("Error Activating helm", err)
	}

	if err := json.Unmarshal(body, &activateHelmRequest); err != nil { // unmarshall body contents as a type Candidate
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Fatalln("Error Activating helm unmarshalling data", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
	success, _ := ActivateHelmChart(activateHelmRequest)
	if !success {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	return
}

//DeactivateHelm chart
func (c *Controller) DeactivateHelm(w http.ResponseWriter, r *http.Request) {

	var helmRequest *pb.HelmRequest
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576)) // read the body of the request
	if err != nil {
		log.Fatalln("Error Activating helm", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if err := r.Body.Close(); err != nil {
		log.Fatalln("Error Activating helm", err)
	}

	if err := json.Unmarshal(body, &helmRequest); err != nil { // unmarshall body contents as a type Candidate
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Fatalln("Error Activating helm unmarshalling data", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
	success, _ := DeactivateHelmChart(helmRequest)
	if !success {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	return
}

//UpdateHelm chart
func (c *Controller) UpdateHelm(w http.ResponseWriter, r *http.Request) {

	var updateHelmRequest *pb.HelmActivateRequest
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576)) // read the body of the request
	if err != nil {
		log.Fatalln("Error Activating helm", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if err := r.Body.Close(); err != nil {
		log.Fatalln("Error Activating helm", err)
	}

	if err := json.Unmarshal(body, &updateHelmRequest); err != nil { // unmarshall body contents as a type Candidate
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Fatalln("Error Activating helm unmarshalling data", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
	success, _ := UpdateHelmChart(updateHelmRequest)
	if !success {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	return
}

//RollbackHelm chart
func (c *Controller) RollbackHelm(w http.ResponseWriter, r *http.Request) {

	var helmRequest *pb.HelmRequest
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576)) // read the body of the request
	if err != nil {
		log.Fatalln("Error Activating helm", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if err := r.Body.Close(); err != nil {
		log.Fatalln("Error Activating helm", err)
	}

	if err := json.Unmarshal(body, &helmRequest); err != nil { // unmarshall body contents as a type Candidate
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Fatalln("Error Activating helm unmarshalling data", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
	success, _ := RollbackHelmChart(helmRequest)
	if !success {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	return
}

//PurgeHelm chart
func (c *Controller) PurgeHelm(w http.ResponseWriter, r *http.Request) {

	var helmRequest *pb.HelmRequest
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576)) // read the body of the request
	if err != nil {
		log.Fatalln("Error Activating helm", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if err := r.Body.Close(); err != nil {
		log.Fatalln("Error Activating helm", err)
	}

	if err := json.Unmarshal(body, &helmRequest); err != nil { // unmarshall body contents as a type Candidate
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Fatalln("Error Activating helm unmarshalling data", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
	success, _ := PurgeHelmChart(helmRequest)
	if !success {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	return
}

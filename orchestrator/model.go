package orchestrator

import "github.com/globalsign/mgo/bson"

//App represents a helm chart deployment
type App struct {
	ID      bson.ObjectId `bson:"_id"`
	Name    string        `json:"name"`
	Version string        `json:"version"`
	Sites   []struct {
		Name string `json:"name"`
	} `json:"sites"`
}

//Apps list
type Apps []App



package orchestrator

import (
	"log"

	"golang.org/x/net/context"
	"google.golang.org/grpc"

	pb "github.com/raghu/EdgeOrchestrator/manager"
)

// ActivateHelmChart calls the RPC method ActivateHelmChart of helm manager
func ActivateHelmChart(helm *pb.HelmActivateRequest) (res bool, error error) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := pb.NewManagerClient(conn)

	resp, err := client.ActivateHelmChart(context.Background(), helm)
	if err != nil {
		log.Fatalf("Could not activate helm chart: %v", err)
		return false, err
	}
	if resp.Success {
		log.Printf("Helm chart is successfully deployed")
		return true, nil

	}
	return true, nil
}

// DeactivateHelmChart calls the RPC method DeactivateHelmChart of helm manager
func DeactivateHelmChart(helm *pb.HelmRequest) (res bool, error error) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := pb.NewManagerClient(conn)
	resp, err := client.DectivateHelmChart(context.Background(), helm)
	if err != nil {
		log.Fatalf("Could not deactivate helm chart: %v", err)
		return false, err
	}
	if resp.Success {
		log.Printf("Helm chart is successfully un deployed")
		return true, nil
	}
	return true, nil
}

// RollbackHelmChart calls the RPC method RollbackHelmChart of helm manager
func RollbackHelmChart(helm *pb.HelmRequest) (res bool, error error) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := pb.NewManagerClient(conn)
	resp, err := client.RollbackHelmChart(context.Background(), helm)
	if err != nil {
		log.Fatalf("Could not rollback helm chart: %v", err)
		return false, err
	}
	if resp.Success {
		log.Printf("Helm chart is successfully rolled back")
		return true, nil
	}
	return true, nil
}

// UpdateHelmChart calls the RPC method UpdateHelmChart of helm manager
func UpdateHelmChart(helm *pb.HelmActivateRequest) (res bool, error error) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := pb.NewManagerClient(conn)
	resp, err := client.UpdateHelmChart(context.Background(), helm)
	if err != nil {
		log.Fatalf("Could not update helm chart: %v", err)
		return false, err
	}
	if resp.Success {
		log.Printf("Helm chart is successfully updated")
		return true, nil
	}
	return true, nil
}

// PurgeHelmChart calls the RPC method DeactivateHelmChart of helm manager
func PurgeHelmChart(helm *pb.HelmRequest) (res bool, error error) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := pb.NewManagerClient(conn)
	resp, err := client.PurgeHelmChart(context.Background(), helm)
	if err != nil {
		log.Fatalf("Could not purgee helm chart: %v", err)
		return false, err
	}
	if resp.Success {
		log.Printf("Helm chart is successfully purged")
		return true, nil
	}
	return true, nil
}

// GetListActiveHelmCharts calls the RPC method ListActiveHelmCharts of manager server
func GetListActiveHelmCharts(filter *pb.HelmFilter) (res bool, error error) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := pb.NewManagerClient(conn)
	// calling the streaming API
	HelmListResponse, err := client.ListActiveHelmCharts(context.Background(), filter)
	if err != nil {
		log.Fatalf("Error on get list of active helm charts: %v", err)
		return false, err
	}

	log.Printf("Helm list: %v", HelmListResponse.ReleaseList)
	return true, nil

}

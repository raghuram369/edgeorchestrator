package orchestrator

import (
	"fmt"
	"log"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

//Repository ...
type Repository struct{}

// SERVER the DB server
const SERVER = "localhost:27017"

// DBNAME the name of the DB instance
const DBNAME = "EdgeOrchestrator"

// DOCNAME the name of the document
const DOCNAME = "apps"

// GetApps returns the list of Apps
func (r Repository) GetApps() Apps {
	session, err := mgo.Dial(SERVER)
	if err != nil {
		fmt.Println("Failed to establish connection to Mongo server:", err)
	}
	defer session.Close()
	c := session.DB(DBNAME).C(DOCNAME)
	results := Apps{}
	if err := c.Find(nil).All(&results); err != nil {
		fmt.Println("Failed to write results:", err)
	}

	return results
}

//GetApp
func (r Repository) GetApp(appname string) App {
	session, err := mgo.Dial(SERVER)
	if err != nil {
		fmt.Println("Failed to establish connection to Mongo server:", err)
	}
	defer session.Close()
	c := session.DB(DBNAME).C(DOCNAME)
	result := App{}
	if err := c.Find(bson.M{"name": appname}).One(&result); err != nil {
		fmt.Println("Failed to write results:", err)
	}

	return result
}

// AddApp inserts an app in the DB
func (r Repository) AddApp(app App) bool {
	session, err := mgo.Dial(SERVER)
	defer session.Close()

	app.ID = bson.NewObjectId()
	session.DB(DBNAME).C(DOCNAME).Insert(app)

	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

// UpdateApp updates an app in the DB (not used for now)
func (r Repository) UpdateApp(app App) bool {
	session, err := mgo.Dial(SERVER)
	defer session.Close()
	session.DB(DBNAME).C(DOCNAME).UpdateId(app.ID, app)

	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

// DeleteApp deletes an App (not used for now)
func (r Repository) DeleteApp(id string) string {
	session, err := mgo.Dial(SERVER)
	defer session.Close()

	// Verify id is ObjectId, otherwise bail
	if !bson.IsObjectIdHex(id) {
		return "NOT FOUND"
	}

	// Grab id
	oid := bson.ObjectIdHex(id)

	// Remove user
	if err = session.DB(DBNAME).C(DOCNAME).RemoveId(oid); err != nil {
		log.Fatal(err)
		return "INTERNAL ERR"
	}

	// Write status
	return "OK"
}

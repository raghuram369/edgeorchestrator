package main

import (
	"log"
	"net/http"

	"github.com/gorilla/handlers"

	"github.com/raghu/EdgeOrchestrator/orchestrator"
)

func main() {
	router := orchestrator.NewRouter() // create routes

	// Allow access from the front-end side to the methods
	allowedOrigins := handlers.AllowedOrigins([]string{"*","http://localhost:9090"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "POST", "DELETE", "PUT","OPTIONS"})
        allowedHeaders := handlers.AllowedHeaders([]string{"Content-Type","access-control-allow-origin","access-control-allow-headers"})
	// launch server with CORS validations
	log.Fatal(http.ListenAndServe(":8999",
		handlers.CORS(allowedOrigins, allowedMethods, allowedHeaders)(router)))
}
